import cv2
import enhance
import funcoes as Funcoes
import pytesseract
import os
import inspect
import datetime
import time

from definicoes import Definicoes
from PIL import Image


''' 
    identifica o candidato 
'''
def identificar(img_rgb):
    x, y = 645, 90
    return ocr_informacao(img_rgb, x, y, 'tmp/identificador.jpg', 120, 32)

''' 
    identifica a prova
'''
def identificar_prova(img_rgb):
    x, y = 650, 150
    info = ocr_informacao(img_rgb, x, y, 'tmp/identificador_prova.jpg', 100, 28)
    return {'identificador': info[:6], 'alternativas': info[6:7], 'questoes': info[7:9]}
    

''' 
    ocr compartilhado 
'''
def ocr_informacao(img_rgb, x, y, path, x1, y1):
    img = img_rgb[y:y + y1, x:x + x1]
    res = cv2.resize(img, None, fx=5, fy=5, interpolation=cv2.INTER_CUBIC)
    #cv2.imwrite('tmp/resize_' + str(x) + '-' + str(y) + '.jpg', res)
    #cv2.rectangle(img_rgb, (x, y), (x + x1, y + y1), (0, 0, 0), 1)
    cv2.imwrite(path, img_rgb)
    #return pytesseract.image_to_string(Image.fromarray(img), config='outputbase digits')
    return pytesseract.image_to_string(Image.fromarray(res), config='--eom 0')

''' 
    Processa cartao  
'''
def processar(path_entrada):
    questoes = []
    path_app = os.path.dirname(os.path.abspath(inspect.stack()[0][1])) + "/"
    path_template_questao = path_app + Definicoes.templateQuestao
    path_template_controle = path_app + Definicoes.templateControle
    path_final = path_app +  'resultados/' 
    arquivo_final = path_final + datetime.datetime.fromtimestamp(time.time()).strftime('%d%m%Y_%H%M%S') + '.jpg'


    print path_template_controle
    
    template_questao = cv2.imread(path_template_questao, 0)
    template_controle = cv2.imread(path_template_controle, 0)
    status = "falha"
    matricula_aluno = ""
    prova = ""

    try:
        entrada = enhance.load_and_adjust_orientation(path_entrada)
        entrada_preparada = Funcoes.preparar_imagem(entrada)
    except:
        raise Exception('Nao foi possivel carregar e preparar a imagem enviada')


    #try:
    #    pontos_controle = Funcoes.detectar_pontos_controle(template_controle, entrada_preparada, arquivo_final, Definicoes.sensibilidadePontoControle)
    #except:
    #    raise Exception('Nao foi possivel detectar os pontos de controle')
    sensibilidadeMinima = 0.6
    sensibilidade = Definicoes.sensibilidadePontoControle    
    while (True):
        try:
            pontos_controle = Funcoes.detectar_pontos_controle(template_controle, entrada_preparada, arquivo_final, sensibilidade)
            break
        except:
            sensibilidade = sensibilidade - 0.01
            if sensibilidade < sensibilidadeMinima:
                raise Exception('Nao foi possivel detectar os pontos de controle')
                

    if Funcoes.quadrante_valido(pontos_controle):

        try:
            img_area_ajustada = Funcoes.ajustar_proporcao(
                entrada_preparada, pontos_controle)
        except:
            raise Exception('Nao foi possivel ajustar proporcao da imagem')


        cv2.imwrite('tmp/area_trabalho.jpg', img_area_ajustada)
        cv2.imwrite('tmp/entrada.jpg', entrada)
        cv2.imwrite('tmp/entrada_preparada.jpg', entrada_preparada)

        try:
            matricula_aluno = identificar(img_area_ajustada.copy())
            prova = identificar_prova(img_area_ajustada.copy())
        except:
            raise Exception('Nao foi possivel identificar dados da prova')


        try:
            imagem_marcada, questoes = Funcoes.detectar_questoes(template_questao, img_area_ajustada, 5)
            status = "concluido"
        except Exception as e:
            raise e #Exception('Nao foi possivel identificar questoes e marcacoes')

    else:
        raise Exception('Pontos de controle invalidos')
              
    cv2.imwrite(arquivo_final, imagem_marcada)
    return status, arquivo_final, questoes, matricula_aluno
