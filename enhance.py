from PIL import Image
from PIL.ExifTags import TAGS
import numpy as np
import argparse
import cv2
import imutils

'''
    Ajusta o nivel de gama na imagem
    return: cv2 Image
'''
def adjust_gamma(image, gamma=1.0):
    return image
    '''
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")

	return cv2.LUT(image, table)
    '''
'''
    Normaliza os tons de cinza da imagem
    return: cv2 Image
'''
def normalize(image):
    img = image
    cv2.normalize(img,  img, 0, 300, cv2.NORM_MINMAX)
    return img

'''
    Carrega a imagem de um path e corrige a orientacao de acordo
    com o exif detectado
    return: cv2 Image

    TODO: Base64 nao carrega informacoes de exif.
'''
def load_and_adjust_orientation(filepath):
    cv_image = cv2.imread(filepath, cv2.CV_LOAD_IMAGE_GRAYSCALE)
    return cv_image