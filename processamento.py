import ocr
import json
import base64
import cStringIO
import datetime
import cv2
import funcoes as Funcoes

from servicos import Servicos
from threading import Thread
from gabarito import Gabarito
from UserString import MutableString
from PIL import Image

class Processamento(Thread):

    def setPath(self, path):
        self.path = path    

    def setDados(self, dados):
        self.dados = dados

    def iniciarSemThread(self):
        return self.processar(False)

    def run(self):
        self.processar(True)

    def processar(self, async_mode = True):


        codigo_siap = Servicos.iniciarProcessamento(self.dados)
        '''
	status, path, resposta, identificador = ocr.processar(self.path)
         
        buffer = cStringIO.StringIO()
        Image.open(path).save(buffer, format='JPEG')
        img_str = base64.b64encode(buffer.getvalue())

        dados = {
            'codigo': codigo_siap,
            'resposta': self.processarLinhaResposta(40, resposta),
            'matricula': identificador,
            'cartaoprocessado': img_str
        }

        Servicos.finalizarProcessamento(dados)
	

        if not async_mode:
            return img_str, identificador, resposta
    
	'''

	return "", "", ""

    @staticmethod
    def processarLinhaResposta(numQuestoes, marcacoes):
        
        lista = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
                 ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
        
        lista = lista[:numQuestoes]
        
        for marcacao in marcacoes:
             for indice, respostas in marcacao.iteritems():
                if indice == "":
                    continue

                questao = int(indice) - 1
                 
                if lista[questao] == ' ':

                    if len(respostas) > 1:
                        lista[questao] = '9'
                    
                    elif len(respostas) == 0:
                        lista[questao] = '0'
                    
                    elif len(respostas) == 1:
                    
                        if respostas[0].strip() == '':
                            lista[questao] = '_'
                    
                        else: 
                            lista[questao] = respostas[0]
                    
        resp = ""
        for i in lista:
            resp = resp + i
        return resp
