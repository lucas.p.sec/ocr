import datetime
import json
from bson.objectid import ObjectId
from mongo import Conexao


class Resposta(object):

    def __init__(self, identificador, marcacoes):
        self.client = Conexao.client()
        self.db = self.client['ocr_database']
        self.identificador = identificador
        self.marcacoes = marcacoes


    def salvar(self):   
        dados = {
            "data": datetime.datetime.utcnow(),
            "identificador": self.identificador,
            "marcacoes": self.marcacoes
        }

        self.db.respostas.insert_one(dados)


    @staticmethod
    def getLast(_id):
        cliente = Conexao.client()
        db = cliente['ocr_database']
        return db.respostas.find({}).sort({"_id": -1}).limit(1)