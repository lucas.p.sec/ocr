import os
import inspect

class Definicoes:

    ''' secret key '''
    secretKey = 'xYvS[NDnc/)>jk[B/,rj,]=?Mn6(&)4=A-vgtPkmw<@x=.P3TV'

    ''' Templates '''
    templateQuestao = 'templates/template_questao.jpg'
    templateControle = 'templates/template_controle.jpg'
    templateMarcacao = 'templates/template_marcacao.jpg'

    ''' Porcentagem de area marcada para considerar a questao marcada '''
    porcentagemMarcacao = 9

    ''' Sensibilidade '''
    sensibilidadeQuestao = 0.8
    sensibilidadePontoControle = 0.68

    ''' Servicos '''
    servicoNovoProcessamento = "http://www.seduce.go.gov.br/servicossiap/Api/ServicoAluno.svc/CartaoRespostaEntrada"
    servicoProcessamentoFinalizado = "http://www.seduce.go.gov.br/servicossiap/Api/ServicoAluno.svc/CartaoRespostaResultado"


    ''' Paths '''
    pathEntrada = os.path.dirname(os.path.abspath(
        inspect.stack()[0][1])) + "/" + 'entrada/'

    pathTmp = os.path.dirname(os.path.abspath(
        inspect.stack()[0][1])) + "/" + 'tmp/'

    pathResultados = os.path.dirname(os.path.abspath(
        inspect.stack()[0][1])) + "/" + 'resultados/'

    pathAplicacao = os.path.dirname(
        os.path.abspath(inspect.stack()[0][1])) + "/"
