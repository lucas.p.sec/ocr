from __future__ import division
from PIL import Image
from threading import Thread
from definicoes import Definicoes
from posicoes import mapearXYProva1

import ocr
import numpy as np
import cv2
import pytesseract
import math
import random


class Questao(Thread):

    def __init__(self, pts, img_rgb, img_bw, alternativas):
        Thread.__init__(self)
        self.questoes = {}
        self.coordenadas = []
        self.pt = pts
        self.img_rgb = img_rgb
        self.alternativas = alternativas
        self.img_bw = img_bw

    def respostas(self):
        return self.questoes

    def marcacoes(self):
        return self.coordenadas

    def possuioMarcacao(self, im):
        black, nblack, total = 0, 0, 0
        for y in range(im.shape[0]):
            for x in range(im.shape[1]):
                total = total + 1

                if im[y][x] <= 180:
                    black = black + 1
                else:
                    nblack = nblack + 1

        porcentagem_marcado = (black * 100) / total
        return porcentagem_marcado >= Definicoes.porcentagemMarcacao
        #return black > nblack
        
    def run(self):
        pt = self.pt

        x, y = pt[0], pt[1]


        ''' OCR titulo '''
        x1, x2 = 10, 40
        y1, y2 = 12, 31
        img_rgb = self.img_rgb

        # img = img_rgb[y + y1:y + y2, x + x1:x + x2]
        # img_pil = Image.fromarray(img)
        
        questao = mapearXYProva1(x, y)

        self.coordenadas.append({
            'i': (x + x2, y + y2),
            'f': (x + x1, y + y1)
        })

        #cv2.imwrite('tmp/xy/x_' + str(x) + ', y_' + str(y) + '.jpg', img)
        img_rgb = self.img_bw

        if not questao in self.questoes:
            self.questoes[questao] = []


        ''' Ret A '''
        x1, x2 = 29, 44
        y1, y2 = 35, 50
        
        rect_a = img_rgb[y + y1:y + y2, x + x1:x + x2]

        if self.possuioMarcacao(rect_a):
           self.coordenadas.append({
               'i': (x + x2, y + y2),
               'f': (x + x1, y + y1)
           })
           self.questoes[questao].append('A')

        ''' Ret B '''
        x1, x2 = 29, 44
        y1, y2 = 56, 71

        rect_b = img_rgb[y + y1:y + y2, x + x1:x + x2]
        if self.possuioMarcacao(rect_b):
            self.coordenadas.append({
                'i': (x + x2, y + y2),
                'f': (x + x1, y + y1)
            })
            self.questoes[questao].append('B')


        ''' Ret C '''
        x1, x2 = 29, 44
        y1, y2 = 77, 92
        rect_c = img_rgb[y + y1:y + y2, x + x1:x + x2]

        if self.possuioMarcacao(rect_c):
            self.coordenadas.append({
                'i': (x + x2, y + y2),
                'f': (x + x1, y + y1)
            })
            self.questoes[questao].append('C')

        ''' Ret D '''
        x1, x2 = 29, 44
        y1, y2 = 98, 113

        ''' correcoes manuais '''
        xCorrecao, yCorrecao = 0, 0

        if questao == '1':
            xCorrecao, yCorrecao = 1, 2 
        elif questao in ['16', '17', '19', '20']:
            xCorrecao, yCorrecao = -2, 2
        elif questao in ['36', '37', '38', '39', '40']:
            xCorrecao, yCorrecao = 0, 2    

        y1, y2 = y1 + yCorrecao, y2 + yCorrecao
        x1, x2 = x1, x2 + xCorrecao

        rect_d = img_rgb[y + y1:y + y2, x + x1:x + x2]
        if self.possuioMarcacao(rect_d):
            self.coordenadas.append({
                'i': (x + x2, y + y2),
                'f': (x + x1, y + y1)
            })
            self.questoes[questao].append('D')


        #if self.alternativas == 5:
        ''' Ret E '''
        x1, x2 = 29, 44
        y1, y2 = 120, 135

        ''' correcoes manuais '''
        xCorrecao, yCorrecao = 0, 0

        if questao == '13':
            xCorrecao, yCorrecao = -2, 1 

        elif questao in ['11', '12', '13', '14', '15']:
            xCorrecao, yCorrecao = -1, 0

        elif questao in ['16', '17', '19', '20']:
            xCorrecao, yCorrecao = -2, 3


        elif questao in ['23', '24', '25']:
            xCorrecao, yCorrecao = -1, 0    

        elif questao in ['36', '37']:
            xCorrecao, yCorrecao = -1, 2

        elif questao in ['38']:
            xCorrecao, yCorrecao = 0, 4

        elif questao in ['39', '40']:
            xCorrecao, yCorrecao = 2, 3


        y1, y2 = y1 + yCorrecao, y2 + yCorrecao
        x1, x2 = x1 + xCorrecao, x2 + xCorrecao

        rect_e = img_rgb[y + y1:y + y2, x + x1:x + x2]
        if self.possuioMarcacao(rect_e):
            self.coordenadas.append({
                'i': (x + x2, y + y2),
                'f': (x + x1, y + y1)
            })
            self.questoes[questao].append('E')
