import cv2
import numpy as np
import pytesseract
import enhance
import time
import base64
import cStringIO
from definicoes import Definicoes
from questao import Questao
from multiprocessing.dummy import Pool as ThreadPool
from matplotlib import pyplot as plt
from PIL import Image

'''
    Procura em uma imagem uma outra imagem de template.
    return: Coordenadas dos pontos combinados
'''
def match(template, match, threshold, alg = False):
    
    if alg == False:
        algo = cv2.TM_CCOEFF_NORMED
    else:
        algo = alg

    res = cv2.matchTemplate(match,template, algo)
    loc = np.where( res >= threshold)
    return zip(*loc[::-1])


'''
    Separa determinados pontos em quadrantes matematicos.
    return: Lista de pontos organizadas por quadrante
'''
def separar_em_quadrantes(imagem, matches):
    quadrantes = [[], [], [], []]
    try:
        h, w = imagem.shape[:2]

        meio_x = w / 2
        meio_y = h / 2
        for ponto in matches:
            if ponto[0] < meio_x and ponto[1] < meio_y:
                quadrantes[0].append(ponto)
            elif ponto[0] > meio_x and ponto[1] < meio_y:
                quadrantes[1].append(ponto)
            elif ponto[0] > meio_x and ponto[1] > meio_y:
                quadrantes[2].append(ponto)
            else:
                quadrantes[3].append(ponto)
        
        return quadrantes
    except:
        return quadrantes
   


'''
    Verifica se os pontos de controle foram detectados corretamente
    return: boolean
'''
def quadrante_valido(quadrante):
    if len(quadrante) == 0:
        return False

    for i in range(0, 3):
        if len(quadrante[0]) == 0:
            return False

    return True


'''
    Aplica efeitos de saturacao, normalizacao e enhance.
    return: imagem tratada
'''
def preparar_imagem(imagem):
    img_corrigida = enhance.normalize(imagem)
    return img_corrigida


'''
    Transforma imagem em base64
'''
def encodeB64(img):
    buffer = cStringIO.StringIO()
    Image.open(img).save(buffer, format='JPEG')
    img_str = base64.b64encode(buffer.getvalue())
    return img_str


'''
    Procura na imagem os pontos de controles
    return: Array com os limites de acordo com os pontos de controle
'''
def detectar_pontos_controle(template, imagem, final, precisao = 0.89):

    w, h = template.shape[::-1]
    pts = match(template, imagem, precisao, cv2.TM_CCOEFF_NORMED)

    for pt in pts:
        cv2.rectangle(imagem, (pt[0], pt[1]), (pt[0] + 20, pt[1] + 20), (0, 0, 255), 1)

    cv2.imwrite('tmp/pontos_controle.jpg', imagem)


   # try:
    quadrante = separar_em_quadrantes(imagem, pts)
    return [quadrante[0][0], quadrante[1][0], quadrante[2][0], quadrante[3][0]]
    #except:
    #    return []


'''
    Ajusta a proporcao de acordo com os pontos
'''
def ajustar_proporcao(imagem, area):
    src = np.float32([area[0], area[1], area[3], area[2]])
    pts = np.float32([[0,0],[800,0],[0,1000],[800,1000]])

    M = cv2.getPerspectiveTransform(src, pts)
    ajustada = cv2.warpPerspective(imagem, M, (800,1000))
    ajustada = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8)).apply(ajustada)

    return ajustada


'''
    Procura na imagem a area das questoes
    return: Coordenadas dos pontos combinados
'''
def detectar_questoes(template, img_rgb, alternativas = 4):
    questoes = []
    resultados = []

    bwi = cv2.threshold(img_rgb, 120, 255, cv2.THRESH_BINARY)[1]

    for pt in match(template, img_rgb, Definicoes.sensibilidadeQuestao, cv2.TM_CCOEFF_NORMED):
        q = Questao(pt, img_rgb, bwi, alternativas)
        q.start()
        questoes.append(q)
        
    while True:
        finalizadas = True
        for t in questoes:
            if t.isAlive():
                finalizadas = False

        if finalizadas:
            break

    for questao in questoes:
        respostas = questao.respostas()
        
        if len(respostas) > 0:
            resultados.append(respostas)
        
        for coord in questao.marcacoes():
            cv2.rectangle(img_rgb, coord['i'], coord['f'], (0, 0, 255), 1)

    cv2.imwrite('tmp/marcadas.jpg', img_rgb)
 
    return img_rgb, resultados

